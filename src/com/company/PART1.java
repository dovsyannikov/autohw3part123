package com.company;

import java.util.Scanner;

public class PART1 {
    /*TASK 1

    We'll say that a number is "teen" if it is in the range 13..19 inclusive.
    Given 2 int values, return true if one or the other is teen, but not both.

    loneTeen(13, 99) → true
    loneTeen(21, 19) → true
    loneTeen(13, 13) → false*/

    public static boolean loneTeen() {
        int a, b;
        a = getInt("введите первое число для таска 1");
        b = getInt("введите второе число для таска 1");
        if ((a >= 13 && a <= 19) || (b >= 13 && b <= 19)){
            if ((a >= 13 && a <= 19) && (b >= 13 && b <= 19)){
                return false;
            }
            if (a == b){
                return false;
            }
            return true;
        }else {
            return false;
        }
    }

    /*TASK 2

    * Given a string, return a new string where the first and last chars have been exchanged.
    frontBack("code") → "eodc"
    frontBack("a") → "a"
    frontBack("ab") → "ba"
    *
    * */

    public static String frontBack() {
        char [] word = getString("Введите слово для таска 2").toCharArray();
        char firstChar = word[0];
        char lastChar = word[word.length - 1];
        word[0] = lastChar;
        word[word.length - 1] = firstChar;
        String result = new String(word);
        return result;
    }

    public static String getString (String message){
        Scanner scanner = new Scanner(System.in);
        System.out.println(message);
        return scanner.next();
    }
    public static int getInt (String message){
        Scanner scanner = new Scanner(System.in);
        System.out.println(message);
        return scanner.nextInt();
    }

    //TASK 3

    /*Given a string, take the first 2 chars and return the string with the 2 chars added at both the front and back,
     so "kitten" yields"kikittenki". If the string length is less than 2, use whatever chars are there.

    front22("kitten") → "kikittenki"
    front22("Ha") → "HaHaHa"
    front22("abc") → "ababcab"*/

    public static String front22() {
        char [] word = getString("введите слово для таска 3(минимум из 2 букв)").toCharArray();
        char firstLetter = word[0];
        char secondLetter = word[1];
        String ab = "" + firstLetter + secondLetter;
        String result = new String(word);
        return ab + result + ab;
    }

    /* TASK 4

    Given a string, if the string "del" appears starting at index 1, return a string where that "del" has been deleted.
    Otherwise, return the string unchanged.

    delDel("adelbc") → "abc"
    delDel("adelHello") → "aHello"
    delDel("adedbc") → "adedbc"*/

    public static String delDel() {
        return getString("введите слово для задачи номер 4").replaceAll("del", "");
    }

    /* TASK 5

    * Given 2 positive int values, return the larger value that is in the range 10..20 inclusive,
     or return 0 if neither is in that range.


    max1020(11, 19) → 19
    max1020(19, 11) → 19
    max1020(11, 9) → 11
    * */

    public static int max1020() {
        int a, b;
        a = getInt("введите первое число для задачи 5");
        b = getInt("введите второе число для задачи 5");

        if ((a > 10 && a <= 20) && (b > 10 && b <= 20)){
            if (a >= b){
                return a;
            } else {
                return b;
            }
        } else if (a > 10 && a <= 20) {
            return a;
        } else if (b > 10 && b <= 20) {
            return b;
        } else {
            return 0;
        }
    }
    /*TASK 6
    * Given 2 ints, a and b, return true if one if them is 10 or if their sum is 10.


    makes10(9, 10) → true
    makes10(9, 9) → false
    makes10(1, 9) → true*/

    public static boolean makes10() {
        int a, b;
        a = getInt("введите первое число для задачи 6");
        b = getInt("введите второе число для задачи 6");
        if (a == 10 || b == 10){
            return true;
        } else if (a+b == 10) {
            {
                return true;
            }
        }else {
            return false;
        }
    }
}
