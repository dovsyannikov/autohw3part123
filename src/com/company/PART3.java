package com.company;

import java.util.Scanner;

public class PART3 {
    private static String getString(String message) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(message);
        return scanner.next();
    }

    private static int getInt(String message) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(message);
        return scanner.nextInt();
    }

    private static boolean getBoolean(String message) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(message);
        return scanner.nextBoolean();
    }
    /* TASK 1
    *
    Given three ints, a b c, return true if it is possible to add two of the ints to get the third.


    twoAsOne(1, 2, 3) → true
    twoAsOne(3, 1, 2) → true
    twoAsOne(3, 2, 2) → false*/

    public static boolean twoAsOne() {
        int a = getInt("Given three ints, a b c, return true if it is possible to add two of the ints to get the third. " +
                "введите 1 число");
        int b = getInt("введите 2 число");
        int c = getInt("введите 3 число");

        if ((a + b == c) || a + c == b || b + c == a) {
            return true;
        } else {
            return false;
        }
    }
    /* TASK 2
    Given 2 ints, a and b, return their sum. However, sums in the range 10..19 inclusive, are forbidden, so in that case just return 20.

    sortaSum(3, 4) → 7
    sortaSum(9, 4) → 20
    sortaSum(10, 11) → 21
    * */

    public static int sortaSum() {
        int a = getInt("Given 2 ints, a and b, return their sum. However," +
                " sums in the range 10..19 inclusive, are forbidden, so in that case just return 20."+ "/n"
        + "Введите 1 число");
        int b = getInt("ВВедите 2 число");
        int sum = a + b;
        if (sum > 10 && sum <= 19){
            return 20;
        }else {
            return sum;
        }
    }

    /* TASK 3




    teenSum(3, 4) → 7
    teenSum(10, 13) → 19
    teenSum(13, 2) → 19*/

    public static int teenSum() {
        int a = getInt("Given 2 ints, a and b, return their sum. However, \"teen\" values in the range 13..19 inclusive, are extra lucky\n" +
                "    . So if either value is a teen, just return 19.");
        int b = getInt("ВВедите 2 число");
        int sum = a + b;
        if (sum >= 13 && sum <= 19){
            return 19;
        }else {
            return sum;
        }
    }
    /*TASK 4
    Your cell phone rings. Return true if you should answer it. Normally you answer,
    except in the morning you only answer if it is your mom calling. In all cases, if you are asleep, you do not answer.


    answerCell(false, false, false) → true
    answerCell(false, false, true) → false
    answerCell(true, false, false) → false*/
    public static boolean answerCell() {
        System.out.println("Your cell phone rings. Return true if you should answer it. Normally you answer, \n" +
                "    except in the morning you only answer if it is your mom calling. In all cases, if you are asleep, you do not answer.\n");
        boolean isMorning = getBoolean("isMorning?");
        boolean isMom = getBoolean("isMom?");
        boolean isAsleep = getBoolean("isAsleep?");
        if (isAsleep){
            return false;
        } else if (isMom){
            return true;
        } else{
            return false;
        }
    }
}
