package com.company;

import java.util.Scanner;

public class PART2 {
    /* TASK 1
    Given a string name, e.g. "Bob", return a greeting of the form "Hello Bob!".


    helloName("Bob") → "Hello Bob!"
    helloName("Alice") → "Hello Alice!"
    helloName("X") → "Hello X!"
    */

    public static String helloName() {
        return getString("Please fill your name");
    }

    private static String getString(String message) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(message);
        return scanner.next();
    }

    private static int getInt(String message) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(message);
        return scanner.nextInt();
    }

    /*TASK 2
    * Given a string, return true if it ends in "ly".

    endsLy("oddly") → true
    endsLy("y") → false
    endsLy("oddy") → false
    * */
    public static boolean endsLy() {
        String str = getString("* Given a string, return true if it ends in \"ly\".");
        char[] chars = str.toCharArray();
        char lastChar = 'y';
        char lastButOneChar = 'l';
        if ((chars[chars.length - 1] == lastChar) && (chars[chars.length - 2] == lastButOneChar)) {
            return true;
        }
        return false;
    }

    /*TASK 3
    Given a string, return a version without both the first and last char of the string. The string may be any length, including 0.

    withouEnd2("Hello") → "ell"
    withouEnd2("abc") → "b"
    withouEnd2("ab") → ""
    * */
    public static String withouEnd2() {
        String str = getString("Given a string, return a version without both the first and last char of the string. " +
                "The string may be any length, including 0.");
        if (str.length() <= 2) {
            return "";
        } else {
            return str.substring(1, str.length() - 1);
        }
    }

    /*TASK 4
    * Given 2 strings, a and b, return a string of the form short+long+short,
    * with the shorter string on the outside and the longer string on the inside.
    * The strings will not be the same length, but they may be empty (length 0).

    comboString("Hello", "hi") → "hiHellohi"
    comboString("hi", "Hello") → "hiHellohi"
    comboString("aaa", "b") → "baaab"
    * */
    public static String comboString() {
        System.out.println("Given 2 strings, a and b, return a string of the form short+long+short,\n" +
                "    * with the shorter string on the outside and the longer string on the inside.\n" +
                "    * The strings will not be the same length, but they may be empty (length 0).");
        String word1 = getString("1 word");
        String word2 = getString("2 word");
        if (word1.length() > word2.length()) {
            return word2 + word1 + word2;
        } else {
            return word1 + word2 + word1;
        }
    }
    /* TASK 5

    Given a string of any length, return a new string where the last 2 chars, if present, are swapped, so "coding" yields "codign".


    lastTwo("coding") → "codign"
    lastTwo("cat") → "cta"
    lastTwo("ab") → "ba"*/

    public static String lastTwo() {
        String str = getString("Given a string of any length, return a new string where the last 2 chars, " +
                "if present, are swapped, so \"coding\" yields \"codign\".");
        if (str.length() < 2) {
            return str;
        } else {
            String str1 = str.substring(0, str.length() - 2);
            StringBuilder sb = new StringBuilder(str.substring(str.length() - 2));
            return str1 + sb.reverse().toString();
        }
    }

    /* TASK 6
    Given a string, if one or both of the first 2 chars is 'x', return the string without those 'x' chars,
    and otherwise return the string unchanged. This is a little harder than it looks.


    withoutX2("xHi") → "Hi"
    withoutX2("Hxi") → "Hi"
    withoutX2("Hi") → "Hi"*/

    public static String withoutX2() {
        StringBuilder stringBuilder = new StringBuilder();
        String str = getString("Given a string, if one or both of the first 2 chars is 'x', return the string without those 'x' chars,\n" +
                "    and otherwise return the string unchanged. This is a little harder than it looks.");
        if (str.length() >= 2){
            char [] chars = str.substring(0, 2).toCharArray();
            char y = 'x';
            for (int i = 0; i < 2; i++) {
                if (y == chars[i]){

                } else {
                    stringBuilder.append(chars[i]);
                }
            }
        }
        return stringBuilder.toString() + str.substring(2);
    }
}
